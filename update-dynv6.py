#!/usr/bin/python3
import urllib.request
import requests # pip install requests
import time
import sys
'''
./your_script token zone
'''
token = sys.argv[1]
zone  = sys.argv[2]

ip = urllib.request.urlopen('https://ident.me').read().decode('utf8') #ident.me is a tool for identifiying your ipv4 address
print(ip)
request = urllib.request.urlopen(f"https://ipv4.dynv6.com/api/update?ipv4={ip}&token={token}&zone={zone}")
