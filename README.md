# wordpress-flo

This is a simple wordpress to celebrate 30 years of **FLO LE PATE**.

## Installation

Please use this link [docker-compose](https://docs.docker.com/compose/install/) to install docker-compose.

## Usage

```bash
mkdir -p /opt/wordpress/
docker-compose up -d
```
## AUTO UPDATE dynv6.com and certificat

```bash
# require lib python "requests"
# example on ubuntu system
apt install python3-pip && pip install requests
rsync ./update-dynv6.py /opt/wordpress/
rsync ./renew-certificate.sh /opt/wordpress
echo "0 6 1 * * bash /opt/wordpress/renew-certificate.sh >> /etc/crontab
echo "0 6 * * * python3 /opt/wordpress/update-dynv6.py YOURTOKEN YOURZONE >/dev/null 2>&1" >> /etc/crontab 
```

## License
ABB (Association des bretons bourrés)

