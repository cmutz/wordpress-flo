#!/bin/bash
# <<branch>> = branch you are pushing to
#rm -rf /root/wordpress-flo/certbot /root/wordpress-flo/db /root/wordpress-flo/nginx /root/wordpress-flo/wordpress
rsync -av --delete /opt/wordpress/certbot /opt/wordpress/db /opt/wordpress/nginx /opt/wordpress/wordpress/wp-content /opt/wordpress/wordpress/wp-config.php /home/ubuntu/wordpress-flo/
sleep 1
git add .
sleep 1
git commit -m "auto commit $(date +"%m-%d-%Y %T")"
sleep 1
git push origin main
