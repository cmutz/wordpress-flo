#!/bin/bash

docker-compose down
sleep 2
certbot certonly --standalone --preferred-challenges http -d x-fl0rianiv3rsair3-x.dns.army
sleep 2
certbot certonly --standalone --preferred-challenges http -d zxcoolman.dns.army
sleep 2
rsync -ave 'ssh' /etc/letsencrypt ubuntu@192.168.1.253:/var/www/nextcloud/ssl/
sleep 2
docker-compose up -d
